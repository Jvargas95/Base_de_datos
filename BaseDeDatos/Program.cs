﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace BaseDeDatos
{
    class Program
    {
        static void Main(string[] args)
        {
            string Cedula;
            string Nombres;
            string Direccion;
            int Telefono;
            string Correo;
            string LugarNacimiento;
            DateTime FechaNacimiento;
            string CodEstudiante;
            DateTime FechaIngreso;
            int CodDocente;
            double Salario;
            int CodNivelA;
            string NivelAcademico = " ";
            int CodCargo;
            string cargo = " ";
            int CodOficio;
            string Oficios = " ";

            string date;
            int opc1, opc2, opc3, opc4, opc5,opc6;

            Oficio of;
            Cargo ca;
            NivelAcademico ni;

            List<Persona> persona = new List<Persona>();
            SqlConnection conn = new SqlConnection(@"Data Source=DESKTOP-K5SL3PQ; Initial Catalog=Estudiantes; Integrated Security=True");
            SqlCommand comando;
            SqlDataReader reader;
            do
            {

                Console.Clear();
                Console.WriteLine("------------Menu de opciones-------------");
                Console.WriteLine("Selecione una Opcion");
                Console.WriteLine("1.Ingrese los Datos:" + "\n" + "2.Mostrar Datos:" + "\n" + "3.Busqueda de Datos:" + "\n" + "4.Borrar Datos:" + "\n" + "0.Salir");
                opc1 = int.Parse(Console.ReadLine());
                switch (opc1)
                {
                    case 1:
                        Console.WriteLine("Ingrese Datos");
                        Console.WriteLine("Cedula: ");
                        Console.ReadLine();
                        Cedula = Console.ReadLine();
                        Console.WriteLine("Nombres: ");
                        Nombres = Console.ReadLine();
                        Console.WriteLine("Direccion: ");
                        Direccion = Console.ReadLine();
                        Console.WriteLine("Telefono: ");
                        Telefono = int.Parse(Console.ReadLine());
                        Console.WriteLine("Correo: ");
                        Correo = Console.ReadLine();
                        Console.WriteLine("Lugar de Nacimiento: ");
                        LugarNacimiento = Console.ReadLine();
                        Console.WriteLine("Fecha de Nacimiento: Dia/Mes/Año");
                        date = Console.ReadLine();

                        FechaNacimiento = Convert.ToDateTime(date);
                        Console.WriteLine("Lista de Niveles Academicos:");
                        conn.Open();
                        comando = new SqlCommand("SELECT *FROM NivelAcademico", conn);
                        reader = comando.ExecuteReader();
                        while (reader.Read())
                        {
                            string CodNivelAc = reader["CodNivelA"].ToString();
                            string NivelAcademic = reader.GetString(1);

                            Console.WriteLine(CodNivelAc + " " + NivelAcademic);
                        }
                        conn.Close();
                        Console.WriteLine("Ingrese el numero del Nivel Academico:");
                        CodNivelA = int.Parse(Console.ReadLine());
                        conn.Open();
                        comando = new SqlCommand("SELECT * FROM NivelAcademico where CodNivelA='" + CodNivelA + "'", conn);
                        reader = comando.ExecuteReader();
                        while (reader.Read())
                        {
                            NivelAcademico = reader.GetString(1);
                        }
                        conn.Close();
                        ni = new NivelAcademico()
                        { CodNivelA = CodNivelA, NivelAcademic = NivelAcademico };
                        Console.WriteLine("1.Codigo Estudiante: " + "\n" + "2.Codigo Docente:");
                        opc2 = int.Parse(Console.ReadLine());
                        switch (opc2)
                        {
                            case 1:
                                Console.ReadLine();
                                Console.WriteLine("Codigo Estudiante:");
                                CodEstudiante = Console.ReadLine();
                                Console.WriteLine("Fecha Ingreso DIA/MES/AÑO:");
                                date = Console.ReadLine();
                                FechaIngreso = Convert.ToDateTime(date);
                                Console.WriteLine("Lista de Oficios:");
                                conn.Open();
                                comando = new SqlCommand("SELECT *FROM Oficio", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodOfi = reader["CodOficio"].ToString();
                                    string Oficio = reader.GetString(1);

                                    Console.WriteLine(CodOfi + " " + Oficio);
                                }
                                conn.Close();
                                Console.WriteLine("Ingrese el numero de Oficio: ");
                                CodOficio = int.Parse(Console.ReadLine());
                                conn.Open();
                                comando = new SqlCommand("SELECT * FROM Oficio Where CodOficio='" + CodOficio + "'", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    Oficios = reader.GetString(1);
                                }
                                conn.Close();
                                of = new Oficio() { CodOficio = CodOficio, Oficios = Oficios };
                                Console.WriteLine("1.Guardar en Lista: " + "\n" + "2.Guardar en BD:");
                                opc4 = int.Parse(Console.ReadLine());
                                switch (opc4)
                                {
                                    case 1:
                                        persona.Add(new Alumnos() { Cedula = Cedula, Nombres = Nombres, Direccion = Direccion, Telefono = Telefono, Correo = Correo, LugarNacimiento = LugarNacimiento, FechaNacimiento = FechaNacimiento, NivelAcademico = ni, CodEstudiante = CodEstudiante, FechaIngreso = FechaIngreso, Oficio = of });
                                        break;

                                    case 2:
                                        conn.Open();
                                        comando = new SqlCommand("INSERT Estudiantes (CodEstudiantes, Cedula, Nombres, Direccion, Telefono, Correo, LugaarNacimiento, FechaNacimiento, FechaIngreso, CodNivelA, CodOficio) VALUES (" + CodEstudiante + ", " + Cedula + ", '" + Nombres + "', '" + Direccion + "', " + Telefono + ", '" + Correo + "', '" + LugarNacimiento + "', '" + string.Format("{0:dd/MM/yyyy}", FechaNacimiento) + "' , '" + string.Format("{0:dd/MM/yyyy}", FechaIngreso) + "', " + CodNivelA + " , " + CodOficio + ")", conn);
                                        comando.ExecuteNonQuery();
                                        conn.Close();
                                        break;
                                    default:
                                        Console.WriteLine("Opcion Invalida");
                                        break;
                                }
                                break;

                            case 2:
                                Console.WriteLine("Codigo Docente:");
                                CodDocente = int.Parse(Console.ReadLine());
                                Console.WriteLine("Salario:");
                                Salario = double.Parse(Console.ReadLine());
                                Console.WriteLine("Lista de Cargos: ");
                                conn.Open();
                                comando = new SqlCommand("SELECT *FROM Cargo", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodCarg = reader["CodCargo"].ToString();
                                    string Cargo = reader.GetString(1);
                                    Console.WriteLine(CodCarg + " " + Cargo);
                                }
                                conn.Close();
                                Console.WriteLine("Ingrese el numero del Cargo; ");
                                CodCargo = int.Parse(Console.ReadLine());
                                conn.Open();
                                comando = new SqlCommand("SELECT * FROM Cargo where CodCargo='" + CodCargo + "'", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    cargo = reader.GetString(1);
                                }
                                conn.Close();
                                ca = new Cargo() { CodCargo = CodCargo, cargo = cargo };
                                Console.WriteLine("1.Guardar en Lista: " + "\n" + "2.Guardar en BD:");
                                opc4 = int.Parse(Console.ReadLine());
                                switch (opc4)
                                {
                                    case 1:
                                        persona.Add(new Docentes() { Cedula = Cedula, Nombres = Nombres, Direccion = Direccion, Telefono = Telefono, Correo = Correo, LugarNacimiento = LugarNacimiento, FechaNacimiento = FechaNacimiento, NivelAcademico = ni, CodDocente = CodDocente, Salario = Salario, cargo = ca });
                                        break;

                                    case 2:
                                        conn.Open();
                                        comando = new SqlCommand("INSERT Docentes (CodDocente, Cedula, Nombres, Direccion, Telefono, Correo, LugaarNacimiento, FechaNacimiento, FechaIngreso, CodNivelA, CodOficio) VALUES (" + CodDocente + ", " + Cedula + ", '" + Nombres + "', '" + Direccion + "', " + Telefono + ", '" + Correo + "', '" + LugarNacimiento + "', '" + string.Format("{0:dd/MM/yyyy}", FechaNacimiento) + "' , " + CodNivelA + " , " + CodCargo + ", " + Salario + ")", conn);
                                        comando.ExecuteNonQuery();
                                        conn.Close();
                                        break;
                                    default:
                                        Console.WriteLine("Opcion Invalida");
                                        break;
                                }
                                break;
                            default:
                                Console.WriteLine("Opcion Invalida");
                                break;
                        }
                        break;


                    //Mostrar Datos
                    case 2:

                        Console.WriteLine("1.Ver Lista:" + "\n" + "2.Ver Estudiantes BD: " + "\n" + "3. Ver Docentes BD:");
                        opc3 = int.Parse(Console.ReadLine());
                        switch (opc3)
                        {
                            case 1:
                                if ((persona == null) || (persona.Count == 0))
                                {
                                    Console.WriteLine("La vista esta vacia.");
                                }
                                else
                                {
                                    foreach (Persona pers in persona)
                                    {
                                        pers.imprimir();
                                    }
                                }
                                Console.ReadKey();
                                break;

                            case 2:
                                Console.WriteLine("Estudiantes: (20)");
                                conn.Open();
                                comando = new SqlCommand("SELECT TOP(20) *FROM Estudiantes order by CodEstudiantes asc", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodEstud = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodEstudiante: " + CodEstud + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);

                                }
                                conn.Close();
                                Console.ReadKey();
                                break;

                            case 3:

                                Console.WriteLine("Docentes (20):");
                                conn.Open();
                                comando = new SqlCommand("SELECT *FROM Docentes order by CodDocente asc", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodDocent = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);
                                    Console.WriteLine("CodDocente: " + CodDocent + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);

                                }
                                conn.Close();
                                Console.ReadKey();
                                break;
                            default:
                                Console.WriteLine("Opcion Invalida");
                                break;
                        }
                        break;

                    //Busqueda de DATOS

                    case 3:
                        Console.WriteLine("1.Buscar Datos Estudiantes" + "\n" + "2.Buscar Datos Docentes" + "\n" + "3.Buscar Lista");
                        opc5 = int.Parse(Console.ReadLine());
                        switch (opc5)
                        {
                            case 1:
                                Console.WriteLine("Estudiantes");
                                Console.WriteLine("Ingrese el Nombre");
                                Nombres = Console.ReadLine();
                                conn.Open();
                                comando = new SqlCommand("SELECT TOP(20) * FROM Estudiantes  where Nombres LIKE '%" + Nombres + "%' OR Nombres LIKE '%" + Nombres + "' order by Nombres asc", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodEstud = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodEstudiante: " + CodEstud + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                }

                                conn.Close();
                                Console.ReadKey();
                                break;

                            case 2:
                                Console.WriteLine("Docentes:");
                                Console.WriteLine("Ingrese el Codigo del Docente:");
                                CodDocente = int.Parse(Console.ReadLine());
                                conn.Open();
                                comando = new SqlCommand("SELECT TOP(20) * FROM Docentes  where CodDocente LIKE '%" + CodDocente + "%' OR CodDocente LIKE '%" + CodDocente + "' order by Nombres asc", conn);
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string Nombr = reader.GetString(2);
                                    string Cedul = reader.GetString(1);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("Nombres: " + Nombr + " Cedula: " + Cedul +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                }

                                conn.Close();
                                Console.ReadKey();
                                break;

                            case 3:
                                if ((persona == null) || (persona.Count == 0))
                                {
                                    Console.WriteLine("La lista esta vacia.");
                                }
                                else
                                {
                                    Console.WriteLine("Ingrese el nombre a buscar:");
                                    Nombres = Console.ReadLine();
                                    foreach (Persona pers in persona)
                                    {
                                        if (pers.Nombres.Contains(Nombres))
                                        {
                                            pers.imprimir();
                                        }

                                    }
                                }
                                Console.ReadKey();
                                break;
                        }

                        break;
                    //Eliminacion de DATOS

                    case 4:
                        Console.WriteLine("1.Borrar datos de la lista" + "\n" + "2. Borrar Estudiantes" + "\n" + "3.Borrar Docente");
                        opc6 = int.Parse(Console.ReadLine());
                        switch (opc6)
                        {
                            case 1:
                                if ((persona == null) || (persona.Count == 0))
                                {
                                    Console.WriteLine("La lista esta vacia.");
                                }
                                else
                                {
                                    Console.WriteLine("Ingrese el nombre a Borroar:");
                                    Nombres = Console.ReadLine();
                                    foreach (Persona pers in persona)
                                    {
                                        if (pers.Nombres.Contains(Nombres))
                                        {
                                            pers.imprimir();
                                        }
                                    }
                                    Console.WriteLine("Esta seguro que desea eliminiar este registro?? S/N: ");
                                    if (Console.ReadLine().ToUpper().Equals("S"))
                                    {
                                        foreach (Persona pers in persona)
                                        {
                                            if (pers.Nombres.Contains(Nombres))
                                            {
                                                persona.Remove(pers);
                                            }
                                            if ((persona == null) || (persona.Count == 0))
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Operacion Cancelada");
                                    }
                                }
                                Console.ReadKey();
                                break;

                            case 2:
                                Console.WriteLine("----------- Estudiantes --------------");
                                Console.ReadLine();
                                Console.WriteLine("Ingrese el Nombre del Estudiante a borrar:");
                                Nombres = Console.ReadLine();
                                conn.Open();
                                comando = new SqlCommand("SELECT *  FROM Estudiantes  where Nombres LIKE '%" + Nombres + "%' order by Nombres asc; ", conn);
                                reader = comando.ExecuteReader();
                                int counter = 0;
                                while (reader.Read())
                                {
                                    string CodEstud = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodEstudiante: " + CodEstud + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                    counter++;
                                }
                                conn.Close();
                                comando = null;
                                reader = null;
                                if (counter != 0)
                                {
                                    conn.Open();
                                    Console.WriteLine("Ingrese el codigo del Estudiante a borrar:");
                                    String cod = Console.ReadLine();
                                    Console.WriteLine("Esta seguro que desea elimnar este registro??  S / N");
                                    if (Console.ReadLine().ToUpper().Equals("S"))
                                    {
                                        comando = new SqlCommand("DELETE  FROM Estudiantes  where CodEstudiantes = '" + cod + "'; ", conn);
                                        comando.ExecuteReader();
                                        Console.WriteLine("Registro Eliminado");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Operacion Cancelada");
                                    }

                                    conn.Close();
                                }
                                else
                                {
                                    Console.WriteLine("NO hay registros que coincidan con el nombre del estudiante");
                                }

                                Console.ReadKey();
                                break;

                            case 3:
                                Console.WriteLine("----------- Docentes --------------");
                                Console.ReadLine();
                                Console.WriteLine("Ingrese el Nombre del Docente a borrar:");
                                Nombres = Console.ReadLine();
                                conn.Open();
                                comando = new SqlCommand("SELECT *  FROM Docentes  where Nombres LIKE '%" + Nombres + "%' order by Nombres asc; ", conn);
                                reader = comando.ExecuteReader();
                                int counter1 = 0;
                                while (reader.Read())
                                {
                                    string CodDocent = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodDocente: " + CodDocent + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                    counter1++;
                                }
                                conn.Close();
                                comando = null;
                                reader = null;
                                if (counter1 != 0)
                                {
                                    conn.Open();
                                    Console.WriteLine("Ingrese el codigo del Docente a borrar:");
                                    String cod = Console.ReadLine();
                                    Console.WriteLine("Esta seguro que desea elimnar este registro??  S / N");
                                    if (Console.ReadLine().ToUpper().Equals("S"))
                                    {
                                        comando = new SqlCommand("DELETE  FROM Docentes  where CodDocente = '" + cod + "'; ", conn);
                                        comando.ExecuteReader();
                                        Console.WriteLine("Registro Eliminado");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Operacion Cancelada");
                                    }

                                    conn.Close();
                                }
                                else
                                {
                                    Console.WriteLine("NO hay registros que coincidan con el nombre del docente");
                                }

                                Console.ReadKey();
                                break;
                        }
                        break;

                    case '0':
                        Console.WriteLine("Muchas Gracias ");
                        break;

                    default:
                        Console.WriteLine("Opcion Invalida");
                        break;
                }
            } while (opc1 != 0);
            Console.ReadKey();
        }
}
}

