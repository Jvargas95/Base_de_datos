﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
namespace BaseDeDatos
{
    class Program
    {

        static void Main(string[] args)
        {
            //string server = "baasu.db.elephantsql.com (baasu-01)";
            //string userId = "cghwjpxo";
            //string password = "CRWSijOgbD0fh6cM22SBkWGbh9Lj8nQ0";
            //int puerto = 5432;

            //NpgsqlConnection conn = new NpgsqlConnection(@"Server=baasu.db.elephantsql.com (baasu-01);Database=cghwjpxo; userId=cghwjpxo; password=CRWSijOgbD0fh6cM22SBkWGbh9Lj8nQ0; port=5432");


            SqlConnection ConexionBD = new SqlConnection(@"Data Source=DESKTOP-K5SL3PQ; Initial Catalog=Estudiantes; Integrated Security=True");
            Conexion(ConexionBD);
        }
        public static void Conexion(DbConnection conec)
        {
            string Nombres;
            int CodDocente;
             
            int opc1, opc2;                   

            List<Persona> persona = new List<Persona>();
            DbCommand comando;
            DbDataReader reader;
            String queryString;
            do
            {

                Console.Clear();
                Console.WriteLine("------------Menu de opciones-------------");
                Console.WriteLine("Selecione una Opcion");
                Console.WriteLine("1.Mostrar Datos:" + "\n" + "2.Busqueda de Datos:" + "\n" + "0.Salir");
                opc1 = int.Parse(Console.ReadLine());
                switch (opc1)
                {
                    
                    //Mostrar Datos
                    case 1:

                        Console.WriteLine("1.Ver Lista:" + "\n" + "2.Ver Estudiantes BD: " + "\n" + "3. Ver Docentes BD:");
                        opc1 = int.Parse(Console.ReadLine());
                        switch (opc1)
                        {
                            case 1:
                                if ((persona == null) || (persona.Count == 0))
                                {
                                    Console.WriteLine("La vista esta vacia.");
                                }
                                else
                                {
                                    foreach (Persona pers in persona)
                                    {
                                        pers.imprimir();
                                    }
                                }
                                Console.ReadKey();
                                break;

                            case 2:
                                Console.WriteLine("Estudiantes: (20)");
                                conec.Open();
                                comando = conec.CreateCommand();
                                queryString = "SELECT TOP(20) *FROM Estudiantes order by CodEstudiantes asc";
                                comando.CommandText = queryString;
                                comando.CommandType = CommandType.Text;
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodEstud = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodEstudiante: " + CodEstud + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);

                                }
                                conec.Close();
                                Console.ReadKey();
                                break;

                            case 3:

                                Console.WriteLine("Docentes (20):");
                                conec.Open();
                                comando = conec.CreateCommand();
                                queryString = "SELECT * FROM Docentes order by CodDocente asc";
                                comando.CommandText = queryString;
                                comando.CommandType = CommandType.Text;
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodDocent = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);
                                    Console.WriteLine("CodDocente: " + CodDocent + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);

                                }
                                conec.Close();
                                Console.ReadKey();
                                break;
                            default:
                                Console.WriteLine("Opcion Invalida");
                                break;
                        }
                        break;

                    //Busqueda de DATOS

                    case 2:
                        Console.WriteLine("1.Buscar Datos Estudiantes" + "\n" + "2.Buscar Datos Docentes" + "\n" + "3.Buscar Lista");
                        opc2 = int.Parse(Console.ReadLine());
                        switch (opc2)
                        {
                            case 1:
                                Console.WriteLine("Estudiantes");
                                Console.WriteLine("Ingrese el Codigo:");
                                Nombres = Console.ReadLine();
                                conec.Open();
                                comando = conec.CreateCommand();
                                queryString = "SELECT TOP(20) * FROM Estudiantes  where CodEstudiantes = '" + Nombres + "' order by Nombres asc";
                                comando.CommandText = queryString;
                                comando.CommandType = CommandType.Text;
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string CodEstud = reader.GetString(0);
                                    string Cedul = reader.GetString(1);
                                    string Nombr = reader.GetString(2);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);

                                    Console.WriteLine("CodEstudiante: " + CodEstud + " Cedula: " + Cedul + " Nombres: " + Nombr +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                }

                                conec.Close();
                                Console.ReadKey();
                                break;

                            case 2:
                                Console.WriteLine("Docentes:");
                                Console.WriteLine("Ingrese el Codigo del Docente:");
                                CodDocente = int.Parse(Console.ReadLine());
                                conec.Open();
                                comando = conec.CreateCommand();
                                queryString = "SELECT TOP(20) * FROM Docentes  where CodDocente = '" + CodDocente + "' order by Nombres asc";
                                comando.CommandText = queryString;
                                comando.CommandType = CommandType.Text;
                                reader = comando.ExecuteReader();
                                while (reader.Read())
                                {
                                    string Nombr = reader.GetString(2);
                                    string Cedul = reader.GetString(1);
                                    string Corr = reader.GetString(5);
                                    string LugarNacim = reader.GetString(6);
                                    Console.WriteLine("Nombres: " + Nombr + " Cedula: " + Cedul +
                                        " Correo: " + Corr + " Lugar de Nacimiento: " + LugarNacim);
                                }

                                conec.Close();
                                Console.ReadKey();
                                break;

                            case 3:
                                if ((persona == null) || (persona.Count == 0))
                                {
                                    Console.WriteLine("La lista esta vacia.");
                                }
                                else
                                {
                                    Console.WriteLine("Ingrese el Codigo a buscar:");
                                    Nombres = Console.ReadLine();
                                    foreach (Persona pers in persona)
                                    {
                                        if (pers.Nombres.Contains(Nombres))
                                        {
                                            pers.imprimir();
                                        }

                                    }
                                }
                                Console.ReadKey();
                                break;
                        }

                        break;

                    case 0:
                        Console.WriteLine("Muchas Gracias ");
                        break;

                    default:
                        Console.WriteLine("Opcion Invalida");
                        break;
                }
            } while (opc1 != 0);
            Console.ReadKey();
        }
}
}

