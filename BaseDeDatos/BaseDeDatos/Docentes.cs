﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos
{
    public class Docentes : Persona
    {
        public int CodDocente { get; set; }

        public double Salario { get; set; }

        public Cargo cargo { get; set; }

        public override void imprimir()
        {
            Console.WriteLine(" CodDocente: " + CodDocente);
            base.imprimir();
            Console.WriteLine(" Salario: " + Salario + " Cargo: " + cargo.cargo);
        }
    }
}
