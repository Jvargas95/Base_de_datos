﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace BaseDeDatos
{
    public class Alumnos : Persona
    {

        public string CodEstudiante { get; set; }

        public DateTime FechaIngreso { get; set; }
        public Oficio Oficio { get; set; }

        public override void imprimir()
        {
            Console.WriteLine(" CodEstudiantes: " + CodEstudiante);
            base.imprimir();
            Console.WriteLine("Fecha Ingreso: " + FechaIngreso + " Oficio: " + Oficio.Oficios);
        }
    }
}
