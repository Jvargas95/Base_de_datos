﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos
{
    public class Persona : IBienvenido<Persona>
    {
       public string Cedula { get; set; }
       public string Nombres { get; set; }
       public string Direccion { get; set; }
       public int Telefono { get; set; }
       public string Correo { get; set; }
       public string LugarNacimiento { get; set; }
       public DateTime FechaNacimiento { get; set; }
       public NivelAcademico NivelAcademico { get; set; }
    
       public virtual void imprimir()
       {
            Console.WriteLine("Cedula:" + Cedula + "Nombres:" + Nombres + "Direccion:" + Direccion +
                              "Telefono:" + Telefono + "Correo:" + Correo + "LugarNacimiento:" + LugarNacimiento +
                              "FechaNacimiento:" + FechaNacimiento + "NivelAcademico:" + NivelAcademico);
       }
                public virtual void mensaje()
        {
        }

    }
}
